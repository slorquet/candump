# README #

This is an application for nuttx to dump all packets on a CAN bus.

### Installation ###

* clone a working nuttx environment
* go to apps/canutils
* git clone this repository
* edit apps/canutils/Kconfig to include the repository directory
* go to nuttx/
* make menuconfig
* select application canutils/candump
* build
* enjoy
