/****************************************************************************
 * canutils/candump/candump_main.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <nuttx/drivers/can.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private types
 ****************************************************************************/

struct can_msg_s canmsg;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void can_setbaud(int fd, int baud)
{
  int ret;
  struct canioc_bittiming_s timings;

  ret = ioctl(fd, CANIOC_GET_BITTIMING, (unsigned long)&timings);
  if(ret!=OK)
    {
      fprintf(stderr, "CANIOC_GET_BITTIMING failed, errno=%d\n", errno);
      return;
    }

  timings.bt_baud = baud;
  ret = ioctl(fd, CANIOC_SET_BITTIMING, (unsigned long)&timings);

  if(ret!=OK)
    {
      fprintf(stderr, "CANIOC_SET_BITTIMING failed, errno=%d\n", errno);
    }
  return;
}

static void set_nonblocking(int fd, bool nb)
{
  int ret;
  int oflags = fcntl(fd, F_GETFL, 0);
  if (oflags == -1)
    {
      fprintf(stderr, "ERROR: fnctl(F_GETFL) failed: %d\n", errno);
    }

  if(nb)
    {
      ret = fcntl(fd, F_SETFL, oflags | O_NONBLOCK);
    }
  else
    {
      ret = fcntl(0, F_SETFL, oflags & ~O_NONBLOCK);
    }

  if (ret < 0)
    {
      fprintf(stderr, "ERROR: fnctl(F_SETFL) failed: %d\n", errno);
    }
}

#ifdef CONFIG_CAN_ERRORS
static void display_error(FAR struct can_msg_s *msg)
{
  printf("Error: %08X\n", msg->cm_hdr.ch_id);
}
#endif

static void display_packet(FAR struct can_msg_s* msg)
{
  int i;
  if(msg->cm_hdr.ch_extid)
    {
      printf("EXT %08X", msg->cm_hdr.ch_id);
    }
  else
    {
      printf("         %03X", msg->cm_hdr.ch_id);
    }

  if(msg->cm_hdr.ch_rtr)
    {
      printf("RTR ");
    }
  else
    {
      printf("    ");
    }

  printf("[%2d]", msg->cm_hdr.ch_dlc);

  for(i=0;i<msg->cm_hdr.ch_dlc;i++)
    {
      printf(" %02X", msg->cm_data[i]);
    }
  printf("\n");
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * media_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int candump_main(int argc, FAR char *argv[])
#endif
{
  char b;
  int fd;
  int ret;
  int baudrate;
  FAR char* devpath;

  /* Open the character driver that wraps the can interface */

  if(argc < 3)
    {
      fprintf(stderr, "CAN <baudrate> <device>\n");
      return 1;
    }

  baudrate = atoi(argv[1]);
  if(baudrate <= 0)
    {
      printf("Invalid baud rate\n");
      return 1;
    }

  devpath = argv[2];
  fd = open(devpath, O_RDWR);

  if (fd < 0)
    {
      fprintf(stderr, "ERROR: failed to open %s: %d\n",
              devpath, errno);
      return 1;
    }

  set_nonblocking(fd, true);
  set_nonblocking(0 , true);

  /* set baud rate */
  can_setbaud(fd, baudrate);

  /* clear input */
  fflush(stdin);

  printf("CAN packet dump utility (baudrate %d)\n", baudrate);

  /* start main loop */
  while(1)
    {
      /*  attempt to read a packet */
      ret = read(fd, &canmsg, sizeof(canmsg) );
      if(ret<0)
        {
          /* error, but wait, EAGAIN is OK if there is nothing to read */
          if(errno != EAGAIN)
            {
              /* real error*/
              printf("Read error on %s, errno=%d\n",devpath, errno);
              break;          
            }
        }
      else if(ret==0)
        {
          /* hmm not sure what happened... means EOF? */
          printf("CAN read() returned zero?\n");
        }

      /* got a packet, display it */
#ifdef CONFIG_CAN_ERRORS
      if(canmsg.cm_hdr.ch_error)
        {
          display_error(&canmsg);
        }
      else
#endif
        {
          display_packet(&canmsg);
        }

      /* stop on keypress */
      ret = read(0, &b, 1);
      if(ret > 0 && b == 0x03)
        {
          printf("Ctrl+C!\n");
          break;
        }
    }

  /* Clean-up and exit */

  close(fd);
  printf("Done.\n");

  set_nonblocking(0 , false);
  return 0;
}

